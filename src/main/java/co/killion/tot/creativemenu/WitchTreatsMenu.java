package co.killion.tot.creativemenu;

import co.killion.tot.ItemResult;
import co.killion.tot.Util;
import io.bfox.anythinginventory.ItemIcon;
import io.bfox.anythinginventory.action.ItemAction;
import io.bfox.anythinginventory.inventory.InventoryMenu;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by bfox1 on 10/24/15.
 */
public class WitchTreatsMenu extends InventoryMenu
{



    public WitchTreatsMenu(int id, JavaPlugin plugin)
    {
        super(Util.WITCH_MENU_NAME + "#" + id, 54, plugin);
    }


    /**
     * Had to override the default restoreInventory method.
     * Default RestoreInventory reloaded based of ItemIcons. Whereas this one neglects itemIcons and
     * turns all current items in inventory into icons and nullifies them.
     */
    @Override
    public void restoreInventory()
    {
        for(ItemStack stack : getInventory().getContents())
        {
            if(stack != null)
            {
                if(getItemIcon(stack) == null)
                {
                    addItemIcon(new ItemIcon(stack, ItemAction.NONE, null));
                }
            }
        }
    }

    /**
     * Default getInventory method restores inventory each time its invoked, therefore,
     * since there isnt any itemstack prior to opening the witch inventory to add items in,
     * it clears it and thus making it empty. This uses the Java's reflection class to obtain the
     * Inventory Field.
     * @return the raw Inventory Field.
     */
    @Override
    public Inventory getInventory()
    {
        try {
            Field field = InventoryMenu.class.getDeclaredField("inventory");
            field.setAccessible(true);
            return (Inventory)field.get(this);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }



}
