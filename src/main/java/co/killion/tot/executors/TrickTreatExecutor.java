package co.killion.tot.executors;

import co.killion.tot.TrickOrTreat;
import co.killion.tot.Util;
import co.killion.tot.WitchEntity;
import co.killion.tot.creativemenu.WitchTreatsMenu;
import io.bfox.anythinginventory.AnythingInventory;
import io.bfox.anythinginventory.inventory.InventoryMenu;
import net.minecraft.server.v1_8_R3.Entity;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Witch;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.List;

/**
 * Created by bfox1 on 10/27/2015.
 * Deuteronomy 8:18
 * 1 Peter 4:10
 */
public class TrickTreatExecutor implements CommandExecutor
{

    private AnythingInventory ai;

    public TrickTreatExecutor(AnythingInventory ai)
    {
        this.ai = ai;
    }
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings)
    {
        Util util = TrickOrTreat.instance.util;

        if(command.getName().equalsIgnoreCase("tot"))
        {
            if(strings.length == 1)
            {
                if(strings[0].equalsIgnoreCase("spawn"))
                {
                    Player player = (Player)commandSender;
                    Entity entity = Util.spawn(player, new WitchEntity(((CraftWorld)player.getWorld()).getHandle()));

                    int id = 0;
                    while(ai.hasMenu(TrickOrTreat.instance, Util.WITCH_MENU_NAME + "#" + id))
                    {
                        id++;
                    }
                    WitchEntity wEntity = (WitchEntity)entity;

                    WitchTreatsMenu menu = new WitchTreatsMenu(id, TrickOrTreat.instance);

                    wEntity.setmenuName(menu.getMenuName());
                    wEntity.setEntityId(id);
                    ai.addMenu(TrickOrTreat.instance, menu);

                    Witch witch = util.getWitchFromEntity(entity);
                    witch.setMetadata(Util.WITCH_METATAG, new FixedMetadataValue(TrickOrTreat.instance, Util.WITCH_METATAG));
                    return true;

                }
                if(strings[0].equalsIgnoreCase("removeAll"))
                {
                    ((Player) commandSender).getWorld().getLivingEntities().stream().filter(entity -> entity.hasMetadata(Util.WITCH_METATAG)).forEach(entity -> {
                        entity.remove();
                        ai.removeMenu(TrickOrTreat.instance, util.getWitchEntityFromWitch((Witch) entity).getMenuName());
                    });
                }
            }
            if(strings.length == 2)
            {
                if(strings[0].equalsIgnoreCase("remove"))
                {
                    try {


                        int id = Integer.valueOf(strings[1]);

                        ((Player)commandSender).getWorld().getLivingEntities().stream().filter(
                                entity -> entity.hasMetadata(Util.WITCH_METATAG) && util.getWitchEntityFromWitch((Witch)entity).getEntityId() == id).forEach(entity ->
                                {
                                    ai.removeMenu(TrickOrTreat.instance, util.getWitchBuildMenu((Witch)entity).getMenuName());
                                    entity.remove();
                                });
                    } catch (NumberFormatException e)
                    {
                        commandSender.sendMessage(ChatColor.RED + "Sorry! but this field must be a digit!");
                    }
                }
            }
        }
        return false;
    }
}
