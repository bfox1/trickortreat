package co.killion.tot;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

/**
 * Created by bfox1 on 10/24/15.
 */
public enum ItemResult
{
    SPIDER_EYE(Material.SPIDER_EYE, false)
            {
                //POISON
                @Override
                public void fireItemResult(Player player)
                {
                    addPotionEffect(player, PotionEffectType.POISON, 1000 , 1);
                }
            },
    ROTTEN_FLESH(Material.ROTTEN_FLESH, false)
            {
                //HUNGER EFFECT
                @Override
                public void fireItemResult(Player player)
                {
                    addPotionEffect(player, PotionEffectType.HUNGER, 2000, 1);
                }
            },
    RED_MUSHROOM(Material.RED_MUSHROOM, false)
            {
                //NAUSEA
                @Override
                public void fireItemResult(Player player)
                {
                    addPotionEffect(player, PotionEffectType.CONFUSION, 1500, 1);
                }
            },
    WEB(Material.WEB, false)
            {
                //SLOWNESS
                @Override
                public void fireItemResult(Player player)
                {
                    addPotionEffect(player, PotionEffectType.SLOW, 1000, 1);
                }
            },
    EYE_OF_ENDER(Material.EYE_OF_ENDER, false)
            {
                //BLINDNESS
                @Override
                public void fireItemResult(Player player)
                {
                    addPotionEffect(player, PotionEffectType.BLINDNESS, 1000, 1);
                }
            },
    SUGAR(Material.SUGAR, true)
            {
                //SPEED
                @Override
                public void fireItemResult(Player player)
                {
                    addPotionEffect(player, PotionEffectType.SPEED, 1500, 1);
                }
            },
    GLOWSTONE(Material.GLOWSTONE_DUST, true)
            {
                //SPEED2
                @Override
                public void fireItemResult(Player player)
                {
                    addPotionEffect(player, PotionEffectType.SPEED, 1000, 2);
                }

            },
    REDSTONE_DUST(Material.REDSTONE, true)
            {
                //SPEED DURATION
                @Override
                public void fireItemResult(Player player)
                {
                    addPotionEffect(player, PotionEffectType.SPEED, 3000, 1);
                }
            };



    private Material material;
    private boolean conditions;

    ItemResult(Material material, boolean conditions)
    {
        this.material = material;
        this.conditions = conditions;
    }


    public Material getMaterial() {
        return material;
    }

    public boolean isConditions() {
        return conditions;
    }

    public static ItemResult getItemResult(ItemStack stack)
    {
        for(ItemResult result : ItemResult.values())
        {
            if(result.getMaterial().equals(stack.getType()))
            {
                return result;
            }
        }
        return null;
    }

    public abstract void fireItemResult(Player player);

    public void addPotionEffect(Player player, PotionEffectType type, int dur, int amp)
    {
        player.addPotionEffect(type.createEffect(dur, amp));
    }
}
