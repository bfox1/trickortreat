package co.killion.tot;

import co.killion.tot.creativemenu.WitchTreatsMenu;
import io.bfox.anythinginventory.AnythingInventory;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftLivingEntity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Witch;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by bfox1 on 10/20/2015.
 * Deuteronomy 8:18
 * 1 Peter 4:10
 */
public class WitchEntity extends EntityWitch
{

    private List<UUID> playerInteractions = new ArrayList<>();

    private String menuName;

    private int entityId;

    public WitchEntity(World world)
    {
        super(world);


        List goalB = (List)Util.getPrivateField("b", PathfinderGoalSelector.class, goalSelector); goalB.clear();
        List goalC = (List)Util.getPrivateField("c", PathfinderGoalSelector.class, goalSelector); goalC.clear();
        List targetB = (List)Util.getPrivateField("b", PathfinderGoalSelector.class, targetSelector); targetB.clear();
        List targetC = (List)Util.getPrivateField("c", PathfinderGoalSelector.class, targetSelector); targetC.clear();

        this.goalSelector.a(1, new PathfinderGoalFloat(this));

        this.goalSelector.a(9, new PathfinderGoalLookAtPlayer(this, EntityHuman.class, 8.0F));

    }


    public void throughPotion()
    {
        Entity entity = this;
        Witch witch = (Witch)entity.getBukkitEntity();

        entity = ((CraftEntity)witch).getHandle();
    }

    public boolean addPlayer(UUID uuid)
    {
        return !hasPlayer(uuid) && playerInteractions.add(uuid);
    }

    public boolean hasPlayer(UUID uuid)
    {
        return playerInteractions.contains(uuid);
    }

    public void purgeWitch()
    {
        playerInteractions.clear();
    }

    public String getMenuName() {
        return menuName;
    }

    public int getEntityId() {
        return entityId;
    }

    public void setmenuName(String name)
    {
        this.menuName = name;
    }

    public void setEntityId(int i)
    {
        this.entityId = i;
    }
}
