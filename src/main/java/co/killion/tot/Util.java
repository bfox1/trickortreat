package co.killion.tot;

import co.killion.tot.creativemenu.WitchTreatsMenu;
import io.bfox.anythinginventory.inventory.InventoryMenu;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftLivingEntity;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftWitch;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Witch;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Created by xbloodrain on 10/24/15.
 */
public class Util {


    public static final String WITCH_MENU_NAME = ChatColor.RESET + "" + ChatColor.GRAY + "[ " + ChatColor.DARK_RED + "Witch" +
            " Menu" + ChatColor.GRAY + " ]";


    public static final String WITCH_METATAG = "wKillion2015";

    /**
     * This Method is for getting Private fields/Methods from a Class.
     * @param fieldName The Field Name
     * @param clazz The Class in which the Field/Method is in.
     * @param object The object to check
     * @return //
     */
    public static Object getPrivateField(String fieldName, Class clazz, Object object)
    {
        Field field;
        Object o = null;

        try
        {
            field = clazz.getDeclaredField(fieldName);

            field.setAccessible(true);

            o = field.get(object);
        }
        catch(NoSuchFieldException e)
        {
            throw new NoSuchFieldError("Field was not found! Looking for " + fieldName + " in " + clazz.getName());
        }
        catch(IllegalAccessException e)
        {
            e.printStackTrace();
        }

        return o;
    }


    /**
     * Registers Custom entities into Bukkit for usage.
     * @param name The Named ID of the Mob.
     * @param id The ID of the Mob.
     * @param nmsClass The Actual Class of the Mob I.E. EntityWolf.class Found in NMS
     * @param customClass The Custom Entity you made.
     */
    public static void registerEntity(String name, int id, Class<? extends EntityInsentient> nmsClass, Class<? extends EntityInsentient> customClass) {
        try {

            /*
            * First, we make a list of all HashMap's in the EntityTypes class
            * by looping through all fields. I am using reflection here so we
            * have no problems later when minecraft changes the field's name.
            * By creating a list of these maps we can easily modify them later
            * on.
            */
            List<Map<?, ?>> dataMaps = new ArrayList<>();
            for (Field f : net.minecraft.server.v1_8_R3.EntityTypes.class.getDeclaredFields()) {
                if (f.getType().getSimpleName().equals(Map.class.getSimpleName())) {
                    f.setAccessible(true);
                    dataMaps.add((Map<?, ?>) f.get(null));
                }
            }

            /*
            * since minecraft checks if an id has already been registered, we
            * have to remove the old entity class before we can register our
            * custom one
            *
            * map 0 is the map with names and map 2 is the map with ids
            */
            try {
                if (dataMaps.get(2).containsKey(id)) {
                    dataMaps.get(0).remove(name);
                    dataMaps.get(2).remove(id);
                }
            }catch (IndexOutOfBoundsException e)
            {
                //
            }

            /*
            * now we call the method which adds the entity to the lists in the
            * EntityTypes class, now we are actually 'registering' our entity
            */
            Method method = net.minecraft.server.v1_8_R3.EntityTypes.class.getDeclaredMethod("a", Class.class, String.class, int.class);
            method.setAccessible(true);
            method.invoke(null, customClass, name, id);

            /*
            * after doing the basic registering stuff , we have to register our
            * mob as to be the default for every biome. This can be done by
            * looping through all BiomeBase fields in the BiomeBase class, so
            * we can loop though all available biomes afterwards. Here, again,
            * I am using reflection so we have no problems later when minecraft
            * changes the fields name
            */
            for (Field f : BiomeBase.class.getDeclaredFields()) {
                if (f.getType().getSimpleName().equals(BiomeBase.class.getSimpleName())) {
                    if (f.get(null) != null) {

                        /*
                        * this peace of code is being called for every biome,
                        * we are going to loop through all fields in the
                        * BiomeBase class so we can detect which of them are
                        * Lists (again, to prevent problems when the field's
                        * name changes), by doing this we can easily get the 4
                        * required lists without using the name (which probably
                        * changes every version)
                        */
                        for (Field list : BiomeBase.class.getDeclaredFields()) {
                            if (list.getType().getSimpleName().equals(List.class.getSimpleName())) {
                                list.setAccessible(true);
                                @SuppressWarnings("unchecked")
                                List<BiomeBase.BiomeMeta> metaList = (List<BiomeBase.BiomeMeta>) list.get(f.get(null));

                                /*
                                * now we are almost done. This peace of code
                                * we're in now is called for every biome. Loop
                                * though the list with BiomeMeta, if the
                                * BiomeMeta's entity is the one you want to
                                * change (for example if EntitySkeleton matches
                                * EntitySkeleton) we will change it to our
                                * custom entity class
                                */
                                for (BiomeBase.BiomeMeta meta : metaList) {
                                    Field clazz = BiomeBase.BiomeMeta.class.getDeclaredFields()[0];
                                    if (clazz.get(meta).equals(nmsClass)) {
                                        clazz.set(meta, customClass);
                                    }
                                }
                            }
                        }

                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Spawns the Enitity to Player Location
     * @param player The player
     * @param entity The Spawning entity.
     * @return Entity
     */
    public static Entity spawn(Player player, Entity entity) {
        net.minecraft.server.v1_8_R3.World mcWorld =  ((CraftWorld) player.getWorld()).getHandle();

        entity.setLocation(player.getLocation().getX(), player.getLocation().getY(),
                player.getLocation().getZ(), player.getLocation().getYaw(), player.getLocation().getPitch());
        ((CraftLivingEntity) entity.getBukkitEntity())
                .setRemoveWhenFarAway(false);
        mcWorld.addEntity(entity, CreatureSpawnEvent.SpawnReason.CUSTOM);
        if(TrickOrTreat.worldName.equals("nothing2015")) {
            TrickOrTreat.worldName = player.getWorld().getName();
        }
        return  entity;
    }


    /**
     * Will attempt to get a Bukkit Witch Entity from Minecraft Entity.
     * @param entity WitchEntity.
     * @return Bukkit Witch Entity.
     */
    public Witch getWitchFromEntity(Entity entity)
    {
        if(entity instanceof WitchEntity)
        {
            return (Witch)entity.getBukkitEntity();
        }
        return null;
    }

    /**
     * Will attempt to return WitchEntity from a Bukkit Witch Entity.
     * @param witch The Bukkit Witch Entity.
     * @return A Witch Entity.
     */
    public WitchEntity getWitchEntityFromWitch(Witch witch)
    {
        System.out.println(((CraftWitch)witch).getHandle());
        System.out.println(witch);
        return  ((WitchEntity)((CraftWitch) witch).getHandle());
    }

    /**
     * Will Attempt to return a WitchTreatMenu
     * @param witch The Witch to be tested.
     * @return WitchMenu
     */
    public WitchTreatsMenu getWitchBuildMenu(Witch witch)
    {
        Object o = ((CraftWitch)witch).getHandle();
        WitchEntity wEntity = null;
        if(o instanceof WitchEntity)
        {
            wEntity = (WitchEntity)o;
        }

        if(wEntity != null)
        {
            return (WitchTreatsMenu)TrickOrTreat.ai.getMenu(TrickOrTreat.instance, wEntity.getMenuName());
        }
        return null;
    }


    /**
     * Removes a specific Witch in the world
     * @param id The ID of the Witch.
     * @param world The World in which the witch is located
     * @return true if successfull
     */
    public boolean removeWitch(int id, World world)
    {
        WitchEntity entity = null;

        for(LivingEntity listEntity : world.getLivingEntities())
        {
            if(listEntity.hasMetadata(Util.WITCH_METATAG))
            {
                WitchEntity witchEntity = (WitchEntity)getWitchEntityFromWitch((Witch)listEntity);
                entity = witchEntity.getId() == id ? witchEntity : null;
            }

        }

        if(entity != null)
        {
            TrickOrTreat.ai.removeMenu(TrickOrTreat.instance, entity.getMenuName());
            world.getLivingEntities().remove(entity);
            return true;
        }
        return false;
    }


    private int getInventoryAmount(Inventory inventory)
    {
        return inventory.getContents().length;
    }

    /**
     * This method is the skin of the Plugin and whats its able to do.
     * After which has been right-clicked on from a player who hasnt already done so.
     * Once Random ItemStack has been selected, it will be ran through the ItemResults.
     *
     * If ItemStack is a ItemResult it will offer special Player Effects.
     * If its a Positive Result, buffs for players advantage, else, will
     * Hinder the players ability to reach more houses.
     *
     * If ItemStack is benefitial Witch will reward run the selectItemStack Method to make a second
     * or third run if another positive Result landed.
     * @param player The Player.
     * @param stack The Random ItemStack.
     * @param inventory The Inventory of the Witch.
     * @return True if result is good or Item isnt a ItemResult.
     */
    private boolean determineResults(Player player, ItemStack stack, Inventory inventory)
    {
        ItemResult result = ItemResult.getItemResult(stack);
        ItemStack nStack = stack.clone();
        if(nStack.getAmount() != 1)
        {
            nStack.setAmount(1);
        }
        if(result != null)
        {
            if(isResultGood(result))
            {
                result.fireItemResult(player);
                player.sendMessage(ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "You done well, Special Treat for you!");
                //selectItemStack(player, inventory);
                player.getInventory().addItem(nStack);
                return true;

            }
            else {
                result.fireItemResult(player);
                player.sendMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "TRICK!");
                return false;
            }

        }
        player.sendMessage(ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "TREAT!");
        player.getInventory().addItem(nStack);
        return true;
    }


    private boolean isResultGood(ItemResult result)
    {
        return result.isConditions();
    }

    /**
     * Chooses an ItemStack from a List to fire when it passes over
     * the determineResults() method.
     * @param player The Player.
     * @param inventory The Inventory.
     */
    public void selectItemStack(Player player, Inventory inventory)
    {

            List<ItemStack> itemList = getShuffledResults(inventory);
            if(itemList.size() != 0) {
                Random random = new Random(itemList.size());
                determineResults(player, itemList.get(0), inventory);
            }
        else
        {
            player.sendMessage(ChatColor.DARK_RED + "It seems as though I have no items to give out.. RIP");
        }

    }

    private List<ItemStack> getShuffledResults(Inventory inventory)
    {
        List<ItemStack> stackList = new ArrayList<>();

        int sl = 0;

        for(ItemStack stack : inventory.getContents())
        {
            if(stack != null)
            for(int i = 0; i < stack.getAmount(); i++)
            {
                stackList.add(i, stack);
            }
        }

        Collections.shuffle(stackList);
        return stackList;

    }









}
