package co.killion.tot;

import co.killion.tot.creativemenu.WitchTreatsMenu;
import co.killion.tot.executors.TrickTreatExecutor;
import io.bfox.anythinginventory.AnythingInventory;
import io.bfox.anythinginventory.init.PluginRegistry;

import net.minecraft.server.v1_8_R3.EntityWitch;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftWitch;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Witch;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Logger;

/**
 * Created by bfox1 on 10/20/2015.
 * Deuteronomy 8:18
 * 1 Peter 4:10
 */
public class TrickOrTreat extends JavaPlugin implements Listener
{

    public static AnythingInventory ai;
    public static TrickOrTreat instance;
    private Logger log ;
    private PluginManager pm;
    public Util util = new Util();

    public static String worldName = "nothing2015";

    public void onEnable()
    {
        this.pm = this.getServer().getPluginManager();
        this.log = this.getLogger();
        ai = (AnythingInventory)pm.getPlugin("AnythingInventory");

        if(ai == null)
        {
            log.severe("Unable to locate the plugin 'AnythingInventory' Disabling Plugin.");
            pm.disablePlugin(this);
        }

        PluginRegistry.initMenuRegistry(this);
        pm.registerEvents(this, this);
        this.getCommand("trickortreat").setExecutor(new TrickTreatExecutor(ai));
        this.getCommand("tot").setExecutor(new TrickTreatExecutor(ai));
        Util.registerEntity("Witch", 66, EntityWitch.class, WitchEntity.class);
        instance = this;
    }

    public void onDisable()
    {

    }


    @EventHandler
    public void onAdminClick(PlayerInteractAtEntityEvent event)
    {
        if(event != null && event.getRightClicked() != null)
        {
            Entity entity = event.getRightClicked();

            if(entity instanceof Witch)
            {
                WitchTreatsMenu menu = util.getWitchBuildMenu((Witch)entity);
                WitchEntity wEntity = null;
                Object o = ((CraftWitch)entity).getHandle();

                if(o instanceof WitchEntity)
                {
                    wEntity = (WitchEntity)o;
                }



                if(menu != null && event.getPlayer().getItemInHand() != null)
                if(event.getPlayer().isOp() && event.getPlayer().getItemInHand().getType().equals(Material.STICK))
                {
                    event.getPlayer().openInventory(menu.getInventory());
                    event.setCancelled(true);
                }
                else
                {
                    if(wEntity != null)
                    {

                        Player player = event.getPlayer();
                        if(!wEntity.hasPlayer(player.getUniqueId()) || player.isOp())
                        {
                            player.sendMessage(ChatColor.GOLD + " " + ChatColor.BOLD + "Trick or Treat...");
                            TrickOrTreat.instance.util.selectItemStack(player, menu.getInventory());

                            wEntity.addPlayer(player.getUniqueId());
                        }
                        else
                        {
                            player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "Hmm... Stop asking me for more Treats!");
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onHit(EntityDamageEvent event)
    {
        if(event != null && event.getEntity() instanceof Witch)
        {
            Witch witch = (Witch)event.getEntity();
            if(event.getCause() != EntityDamageEvent.DamageCause.CUSTOM && event.getCause() != EntityDamageEvent.DamageCause.SUICIDE)
            if(witch.hasMetadata(Util.WITCH_METATAG))
            {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onWitchOpenEvent(InventoryCloseEvent event)
    {
        if(event != null && ai.hasMenu(this, event.getInventory().getName()))
        {
            WitchTreatsMenu menu = (WitchTreatsMenu)ai.getMenu(this, event.getInventory().getName());
            menu.restoreInventory();
        }
    }
}
